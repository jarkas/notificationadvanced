<?php
/**
 * Created by PhpStorm.
 * User: ahmad.jarkas
 * Date: 30.07.2019
 * Time: 20:39
 */

namespace NotificationAdvanced;


use Shopware\Components\Plugin\Context\ActivateContext;
use Shopware\Components\Plugin\Context\DeactivateContext;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;
use Shopware\Components\Plugin\Context\UpdateContext;


class NotificationAdvanced extends \Shopware\Components\Plugin
{

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function install(InstallContext $context)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function activate(ActivateContext $context)
    {
        // on plugin activation clear the cache
        $context->scheduleClearCache(ActivateContext::CACHE_LIST_ALL);
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function uninstall(UninstallContext $context)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function update(UpdateContext $context)
    {

        $context->scheduleClearCache(UpdateContext::CACHE_LIST_ALL);
    }


    public function deactivate(DeactivateContext $context)
    {
        // on plugin deactivation clear the cache
        $context->scheduleClearCache(DeactivateContext::CACHE_LIST_ALL);
    }

}