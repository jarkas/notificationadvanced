<?php
/**
 * Created by PhpStorm.
 * User: ahmad.jarkas
 * Date: 30.07.2019
 * Time: 20:50
 */

namespace NotificationAdvanced\Subscriber;

use Doctrine\Common\Collections\ArrayCollection;
use Enlight\Event\SubscriberInterface;
use Enlight_Event_EventArgs;
use Enlight_Template_Manager;
use Shopware\Components\Theme\LessDefinition;


class Frontend implements SubscriberInterface
{
    private $pluginDirectory;

    /**
     * @param $pluginDirectory
     * @param Enlight_Template_Manager $templateManager
     */

    public function __construct($pluginDirectory, Enlight_Template_Manager $templateManager)
    {
        $this->pluginDirectory = $pluginDirectory;

        $this->templateManager = $templateManager;
    }


    public static function getSubscribedEvents()
    {


        return [
            'Enlight_Controller_Action_PostDispatchSecure_Frontend_Detail' => 'extendFrontend',
            'Theme_Compiler_Collect_Plugin_Javascript' => 'NotificationAdvancedOnCollectJavascriptFiles',
            'Theme_Compiler_Collect_Plugin_Less' => 'NotificationAdvancedOnCollectFiles',

        ];

    }

    public function extendFrontend(\Enlight_Controller_ActionEventArgs $args)
    {

        $subject = $args->getSubject();
        $request = $args->getSubject()->Request();
        $view = $subject->View();


        $view->addTemplateDir(__DIR__ . '/../Resources/Views');

        $shop   =   Shopware()->Shop();
        $config = Shopware()->Container()->get('shopware.plugin.cached_config_reader')->getByPluginName('NotificationAdvanced', $shop );


        $NotificationAdvanced = [];
        foreach ($config as $key => $value)
        {
            $NotificationAdvanced[$key] = str_replace('"','',$value);
        }

        $view->assign('NotificationAdvanced', $NotificationAdvanced);


//      get user email

        $customerEmail = Shopware()->Modules()->Admin()->sGetUserMailById();
        $view->assign('customerEmail', $customerEmail);


    }



    public function NotificationAdvancedOnCollectFiles(\Enlight_Event_EventArgs $args)
    {
        return new LessDefinition(
            [],
            [__DIR__ . '/../Resources/Views/frontend/_public/src/less/all.less',]
        );

    }

    public function NotificationAdvancedOnCollectJavascriptFiles(\Enlight_Event_EventArgs $args)
    {
        $jsDir = __DIR__ . '/../Resources/Views/frontend/_public/src/js/notification_advanced.js';
        return new ArrayCollection( array($jsDir) );

    }

}