{extends file="parent:frontend/plugins/notification/index.tpl"}

{block name="frontend_detail_index_notification_form"}

    {if !$NotifyAlreadyRegistered}

        {if $NotificationAdvanced['notification_advanced_layout'] == 'notification_advanced_full_mail' && $customerEmail}

            <form method="post" action="{url action='notify' sArticle=$sArticle.articleID number=$sArticle.ordernumber}" class="notification--form block-group">
                <input type="hidden" name="notifyOrdernumber" value="{$sArticle.ordernumber}" />
                {block name="frontend_detail_index_notification_field"}
                    <input {if $customerEmail}value="{$customerEmail}"{/if} name="sNotificationEmail" type="email" class="notification--field block" placeholder="{s name='DetailNotifyLabelMail'}{/s}" />
                {/block}

                {block name="frontend_detail_index_notification_button"}
                    <button type="submit" class="notification--button btn is--center block">
                        <i class="icon--mail"></i>
                    </button>
                {/block}

                {* Data protection information *}
                {block name="frontend_detail_index_notification_privacy"}
                    {if {config name=ACTDPRTEXT} || {config name=ACTDPRCHECK}}
                        {include file="frontend/_includes/privacy.tpl"}
                    {/if}
                {/block}
            </form>

        {elseif $NotificationAdvanced['notification_advanced_layout'] == 'notification_advanced_button' && $customerEmail}

            <form method="post" action="{url action='notify' sArticle=$sArticle.articleID number=$sArticle.ordernumber}" class="notification--form block-group">
                <input type="hidden" name="notifyOrdernumber" value="{$sArticle.ordernumber}" />
                {block name="frontend_detail_index_notification_field"}
                    {if $customerEmail}
                        <span>
                           {s name='NotificationAdvancedCheckbob'}Benachrichtige mich wenn der Artikel wieder verfügbar ist{/s}
                        </span>
                        <label>
                            <input value="{$customerEmail}" name="sNotificationEmail" type="checkbox" onclick="this.form.submit()" class="notification--field block"/>
                        </label>
                    {else}
                            <input name="sNotificationEmail" type="email" class="notification--field block" placeholder="{s name='DetailNotifyLabelMail'}{/s}" />
                    {/if}
                {/block}

                {* Data protection information *}
                {block name="frontend_detail_index_notification_privacy"}
                    {if {config name=ACTDPRTEXT} || {config name=ACTDPRCHECK}}
                        {include file="frontend/_includes/privacy.tpl"}
                    {/if}
                {/block}
            </form>

        {elseif $NotificationAdvanced['notification_advanced_layout'] == 'notification_advanced_checkbox' && $customerEmail}

            {*TODO*}

            {*Button Layout: Benachrichtigung beim klicken eines Buttons*}

            {else}
            {$smarty.block.parent}
        {/if}
    {/if}

{/block}

